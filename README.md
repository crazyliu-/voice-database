# 语音库

#### 介绍
爱阅书香语音库

#### 使用

在iOS浏览器中[打开地址](https://gitee.com/ift123/voice-database/tree/master)

选择你喜欢的语音库

点进进入，再“下载此文件”

![如下图](https://images.gitee.com/uploads/images/2020/1215/121611_f6b20099_2338311.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1215/121750_30340b21_2338311.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1215/121814_d0fb0448_2338311.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1215/165223_92de4a3c_2338311.png "屏幕截图.png")